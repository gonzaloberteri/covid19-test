import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {render, fireEvent} from '@testing-library/react-native';
import {act} from 'react-test-renderer';
import Questions from '../src/constants/questions';

import AppNavigator from '../App';

// Silence the warning https://github.com/facebook/react-native/issues/11094#issuecomment-263240420
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

describe('Healthy page is show after finishing test with no "yes" answers', () => {
  test('Home screen renders correcly', async () => {
    const component = <AppNavigator />;

    const result = render(component).toJSON();
    await act(async () => {
      expect(result).toMatchSnapshot();
    });
  });

  test('Clicking the begin button takes to the first question', async () => {
    const component = <AppNavigator />;

    const result = render(component);
    const toClick = await result.findByText('Begin');

    fireEvent(toClick, 'press');
    const firstQuestion = await result.findByText(Questions[0].text);

    expect(firstQuestion).toBeTruthy();
  });

  test('Answering "no" to all should route to result screen', async () => {});
  test('Health test should be passed and screen should be green', async () => {});
});

describe('Infected page is show after finishing test with al least one "yes" answer', () => {
  test('Clicking the begin button takes to the first question', async () => {
    const component = <AppNavigator />;

    const result = render(component);
    const toClick = await result.findByText('Begin');

    fireEvent(toClick, 'press');
    const firstQuestion = await result.findByText(Questions[0].text);

    expect(firstQuestion).toBeTruthy();
  });

  test('Answering "yes" to one or more should route to result screen', async () => {});
  test('Health test should be failed and screen should be red', async () => {});
});
