import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  GestureResponderEvent,
} from 'react-native';

type buttonType = {
  label: String;
  onPress: (event: GestureResponderEvent) => void;
};

const CustomButton = (props: buttonType) => {
  return (
    <TouchableOpacity style={styles.begin} onPress={props.onPress}>
      <Text style={styles.beginText}>{props.label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  begin: {
    borderRadius: 50,
    backgroundColor: '#409bfa',
    marginHorizontal: 16,
    marginBottom: 32,
    paddingHorizontal: 20,
    paddingVertical: 14,
  },
  beginText: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Inter-Bold',
  },
});

export default CustomButton;
