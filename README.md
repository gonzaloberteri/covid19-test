# Gotchas

Ios build may not work out of the box, following should help
```shell
cd ios && pod install && cd ..
```

# TODO
- [ ] Buttons should be disabled on touch @ componentDidMount()
- [ ] Pressing back button while on questionnaire should pop() the last answer from the array
- [ ] Once questionnaire is finished, pressing back button should route to the homescreen
- [ ] Complete remaining tests