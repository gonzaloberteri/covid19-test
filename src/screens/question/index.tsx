import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import AnswerButton from '../../components/answerButton';
import Container from '../../components/container';
import Highlighter from 'react-native-highlight-words';
import Routes from '../../navigation/routes';
import Questions from '../../constants/questions';

const QuestionScreen = ({route, navigation}: any): JSX.Element => {
  const answers: Boolean[] = route.params.answers || [];
  const questionId: number = route.params.questionId || 0;

  /* Cancel button, exits questionnaire, resets screen stack */
  const returnHome = () => {
    navigation.reset({
      index: 0,
      routes: [{name: Routes.HOME_SCREEN}],
    });
  };

  /* Add answer to answers array */
  const nextQuestion = (answer: Boolean) => {
    answers.push(answer);

    /* 1 or more questions left, ask new questions */
    if (questionId < Questions.length - 1) {
      navigation.push(Routes.QUESTION_SCREEN, {
        questionId: questionId + 1,
        answers: answers,
      });
      /* All questions answered, check result */
    } else {
      navigation.navigate(Routes.RESULT_SCREEN, answers);
    }
  };

  return (
    <Container>
      <Text style={styles.cancel} onPress={returnHome}>
        Cancel
      </Text>

      <View style={styles.questionContainer}>
        <Highlighter
          style={styles.question}
          highlightStyle={styles.bold}
          searchWords={Questions[questionId].bold || []}
          textToHighlight={Questions[questionId].text}
        />
      </View>

      <View style={styles.buttonContainer}>
        <AnswerButton isHappy={true} onPress={nextQuestion} />
        <AnswerButton
          isHappy={false}
          onPress={nextQuestion}
          marginStart={{marginStart: 16}}
        />
      </View>

      <View style={styles.questionCount}>
        <Text style={styles.questionCountText}>
          Question {questionId + 1} of {Questions.length}
        </Text>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  cancel: {
    fontSize: 16,
    color: '#0a7ffe',
    backgroundColor: '#00000000',
    marginStart: 10,
    marginTop: 16,
    fontFamily: 'Inter-SemiBold',
  },
  questionContainer: {
    display: 'flex',
    minHeight: 350,
    flexGrow: 1,
  },
  question: {
    fontSize: 22,
    lineHeight: 34,
    marginHorizontal: 37,
    fontFamily: 'Inter-SemiBold',
    marginTop: 'auto',
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignSelf: 'stretch',
    textAlign: 'center',
    marginTop: 30,
    marginStart: 37,
  },
  questionCount: {
    backgroundColor: '#f9f9f9',
    borderRadius: 50,
    alignSelf: 'baseline',
    paddingHorizontal: 12,
    marginHorizontal: 37,
    marginTop: 70,
    marginBottom: 30,
  },
  questionCountText: {
    color: '#a5a5a5',
    fontFamily: 'Inter-SemiBold',
  },
  bold: {
    fontFamily: 'Inter-Black',
    fontWeight: '800',
  },
});

export default QuestionScreen;
